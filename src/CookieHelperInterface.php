<?php

namespace Drupal\remember;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for Remember Cookie Helper services.
 */
interface CookieHelperInterface {

  /**
   * Returns the name of the remember cookie.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return string
   *   The cookie name.
   */
  public function getCookieName(Request $request);

  /**
   * Returns the value of the remember cookie.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return string
   *   The cookie value.
   */
  public function getCookieValue(Request $request);

  /**
   * Checks if a request contains a remember cookie.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return bool
   *   TRUE if the request provides a remember cookie.
   */
  public function hasCookie(Request $request);

}
