<?php

namespace Drupal\remember;

/**
 * Defines an exception for remember tokens.
 */
class TokenException extends \RuntimeException {

}
